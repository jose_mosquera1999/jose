    <script src="public/plugins/jquery/jquery-1.12.4.min.js"></script>
    <script src="public/plugins/bootstrap-3.3.7-dist/js/bootstrap.min.js" ></script>
    <script src="public/js/holder.min.js" ></script>



    <script>
        $(function() {
        	if (typeof activeApp !== 'undefined') {
        		//console.log(activeAppMenu);

        		// resalta el link en el menu
				$('.nav-sidebar > li.' + activeApp).addClass("active");

				$("button").click(function(){

				    $.ajax({
				    	url: "http://localhost/GitDeploy/Deploy/deploy", // la URL para la petición
				    	data : { app : activeApp }, // la información a enviar en formato json
				    	type : 'POST', // especifica si será una petición POST o GET
				    	dataType : 'json', // el tipo de información que se espera de respuesta

				    	// código a ejecutar si la petición es satisfactoria;
					    // la respuesta es pasada como argumento a la función
					    success : function(json) {
					        /*
					        $('<h1/>').text(json.title).appendTo('body');
					        $('<div class="content"/>')
					            .html(json.html).appendTo('body');
					        */
					        console.log(json);
					    },

					    // código a ejecutar si la petición falla;
					    // son pasados como argumentos a la función
					    // el objeto de la petición en crudo y código de estatus de la petición
					    error : function(xhr, status) {
					        console.log('Disculpe, existió un problema');
					    },

					    // código a ejecutar sin importar si la petición falló o no
					    complete : function(xhr, status) {
					        console.log('Petición realizada');
					    }

					});
				});
			}
            
        });
    </script>

    </body>

</html>