        <!-- navbar -->
        <nav class="navbar navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">                    
                    <a class="navbar-brand" href="http://localhost/TallerGit/">
                        <img src="public/images/git_windows_logo.png" width="45px">
                        <span class="navbar-brand-text">
                            Taller Git                           
                        </span>                        
                    </a>
                </div>

                <div id="navbar" class="navbar-collapse collapse">
                    
                    <ul class="nav navbar-nav ">
                        <li><a href="#">Menu 1</a></li>
                        <li><a href="#">Menu 2</a></li>
                        <li><a href="#">Menu 3</a></li>
                        <li><a href="#">Menu 4</a></li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">
                                <?php echo USUARIO . ' - ' . NOMBRE_USUARIO; ?> 
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="http://localhost/TallerGit/">Cerrar Sesion</a></li>
                            </ul>
                        </li>
                    </ul>

                </div>


                
            </div>
        </nav>
        <!-- /navbar -->